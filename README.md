# Chebur-ansible-GB



## Cравнительная таблица систем управления конфигурациями.

| Название | Плюсы | Минусы|
| :---: | :------ | :------|
| CFEngine |1.Первый в своем роде долше всех на рыке|1.Высокий порог вхождений<br>2.Написан на C как следствие сложно расширять  |
| Chef | 1.Быстрота развертования<br>2.Гибкость настроек<br>3.Мультиплатформенность |1.Громоздкость (Рецепты Chef зачастую достаточно объемны)<br>2.Производительность(уступает конкурентам в производительности и потреблении ресурсов рабочей станции.) |
|Puppet|1.Свой язык манифестов<br>2.Высокая популярность(развитое комьюнити)|1.Свой язык манифестов<br>2.Требует установки агента на клиентской машине. |
|SaltStack|1.Гибкость в использование, можно испаользовать как PUSH так PULL стратегии.<br>2.Гибкое масштабирование|1.Не так распространен как Ansible или Pupet|
|Ansible|1.Не требует установки клиента<br>2.Переносимость конфигураций<br>3.Низкий уровень вхождения<br>4.На данный  момент самы популярный SCM как следствие развитое комьюнити|1.Использует только PUSH стратегию, что услажняет работу при больщом количестве клиентов.|
